<?php
/**
 * Created by PhpStorm.
 * User: ProgKP
 * Date: 25.12.16 17:55
 */

namespace AppBundle\Form\Type;



use FOS\RestBundle\Form\Transformer\EntityToIdObjectTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $authorTransformer = new EntityToIdObjectTransformer($options['em'], 'AppBundle:Author');
        $builder
            ->add('nameBook',TextType::class)
            ->add('colorBook',TextType::class)
            ->add($builder->create('author', TextType::class)->addModelTransformer($authorTransformer))
            ->add('datePublish', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('cityPublish', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
          'data_class' => 'AppBundle\Entity\Book',
          'em' => null,
        ]);
    }

    public function getName()
    {
        return 'book';
    }

}