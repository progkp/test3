<?php
/**
 * Created by PhpStorm.
 * User: ProgKP
 * Date: 25.12.16 16:33
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use AppBundle\Form\Type\AuthorType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AuthorController
 * @package AppBundle\Controller
 *
 * @RouteResource("author")
 */
class AuthorController extends FOSRestController implements ClassResourceInterface
{

    /**
     * @param Author $author
     * @return Author
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Author",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function getAction(Author $author)
    {
        return $author;
    }


    /**
     * @param ParamFetcherInterface $paramFetcher
     * @return \AppBundle\Entity\Author[]|array
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Author[]",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     * @QueryParam(name="limit", requirements="\d+", default="10", description="the max number of records to return")
     * @QueryParam(name="offset", requirements="\d+", nullable=true, default="0", description="the record number to start results at")
     *
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = $paramFetcher->get('offset');

        return $this->getDoctrine()->getRepository("AppBundle:Author")->findBy([],[], $limit, $offset);
    }

    /**
     * @param Request $request
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Author[]",
     *     statusCodes={
     *         201 = "Returned when a new Author has been successful created",
     *         400 = "Return when errors"
     *     }
     * )
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(AuthorType::class, null);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $author = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $em->persist($author);
        $em->flush();

        $routeOptions = [
            'author' => $author->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_author', $routeOptions, Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param Author $author
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Author[]",
     *     statusCodes={
     *         201 = "Returned when a Book has been successful updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function putAction(Request $request, Author $author)
    {
        $form = $this->createForm(AuthorType::class, $author);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
          'author' => $author->getId(),
          '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_author', $routeOptions, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @param Author $author
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Author[]",
     *     statusCodes={
     *         201 = "Returned when a Book has been successful updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function patchAction(Request $request, Author $author)
    {
        $form = $this->createForm(AuthorType::class, $author);
        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'author' => $author->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_author', $routeOptions, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Author $author
     * @return View
     *
     *
     * @ApiDoc(
     *     statusCodes={
     *         204 = "Returned when an existing Book has been successful deleted",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function deleteAction(Author $author)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($author);
        $em->flush();

        return new View(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Author $author
     * @return \AppBundle\Entity\Book[]|array
     *
     * @ApiDoc(
     *
     *     output="AppBundle\Entity\Book[]",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function getBookAction(Author $author)
    {
        return $this->getDoctrine()->getRepository("AppBundle:Book")->findBy(['author' => $author]);
    }

}