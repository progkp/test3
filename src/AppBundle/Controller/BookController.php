<?php
/**
 * Created by PhpStorm.
 * User: ProgKP
 * Date: 25.12.16 15:46
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Form\Type\BookType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class BookController
 * @package AppBundle\Controller
 *
 * @RouteResource("book")
 */
class BookController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @param Book $book
     * @return Book
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Author",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function getAction(Book $book)
    {
        return $book;
    }

    /**
     * @param ParamFetcherInterface $paramFetcher
     * @return \AppBundle\Entity\Book[]|array
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Book[]",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     * @QueryParam(name="limit", requirements="\d+", default="10", description="the max number of records to return")
     * @QueryParam(name="offset", requirements="\d+", nullable=true, default="0", description="the record number to start results at")
     *
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = $paramFetcher->get('offset');

        return $this->getDoctrine()->getRepository("AppBundle:Book")->findBy([],[], $limit, $offset);
    }

    /**
     * @param Request $request
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\Form
     *
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Author[]",
     *     statusCodes={
     *         201 = "Returned when a new Author has been successful created",
     *         400 = "Return when errors"
     *     }
     * )
     */
    public function postAction(Request $request)
    {
        $form  = $this->createForm(BookType::class, null, [
            'em' => $this->getDoctrine()->getManager(),
        ]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $book = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $em->persist($book);
        $em->flush();

        $routeOptions = [
            'book' => $book->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_book', $routeOptions, Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param Book $book
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\Form
     *

     * @ApiDoc(
     *     output="AppBundle\Entity\Author[]",
     *     statusCodes={
     *         201 = "Returned when a Author has been successful updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function putAction(Request $request, Book $book)
    {
        $form  = $this->createForm(BookType::class, $book, [
            'em' => $this->getDoctrine()->getManager(),
        ]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'book' => $book->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_book', $routeOptions, Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param Book $book
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\Form
     *
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Author[]",
     *     statusCodes={
     *         201 = "Returned when a Author has been successful updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function patchAction(Request $request, Book $book)
    {
        $form  = $this->createForm(BookType::class, $book, [
            'em' => $this->getDoctrine()->getManager(),
        ]);
        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'book' => $book->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_book', $routeOptions, Response::HTTP_CREATED);
    }

    /**
     * @param Book $book
     * @return View
     *
     *
     * @ApiDoc(
     *     statusCodes={
     *         204 = "Returned when an existing Author has been successful deleted",
     *         404 = "Return when not found"
     *     }
     * )
     *
     */
    public function deleteAction(Book $book)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($book);
        $em->flush();

        return new View(null, Response::HTTP_NO_CONTENT);
    }

}