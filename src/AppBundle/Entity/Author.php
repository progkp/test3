<?php
/**
 * Created by PhpStorm.
 * User: ProgKP
 * Date: 25.12.16 10:12
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMSSerializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="author")
 * @JMSSerializer\ExclusionPolicy("all")
 */
class Author
{
    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMSSerializer\Expose
     */
    protected $id;

    /**
     * @var string $firstName
     *
     * @ORM\Column(type="string", name="firstName")
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * @Assert\NotBlank()
     * @JMSSerializer\Expose
     * @JMSSerializer\SerializedName("firstName")
     */
    protected $firstName;

    /**
     * @var string $lastName
     *
     * @ORM\Column(type="string", name="lastName")
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     * )
     * @Assert\NotBlank()
     * @JMSSerializer\Expose
     * @JMSSerializer\SerializedName("lastName")
     */
    protected $lastName;

    /**
     * @var date $birthday
     *
     * @ORM\Column(type="date", name="birthday")
     * @Assert\NotBlank()
     * @JMSSerializer\Expose
     * @JMSSerializer\SerializedName("birthday")
     * @JMSSerializer\Type("DateTime<'Y-m-d'>")
     */
    protected $birthday;

    /**
     * @var date $dateOfDeath
     *
     * @ORM\Column(type="date", name="dateOfDeath")
     * @Assert\NotBlank()
     * @Assert\Expression(
     *     "this.getBirthday() < value",
     *     message="Date of birth must be less than the date of death"
     * )
     * @JMSSerializer\Expose
     * @JMSSerializer\SerializedName("dateOfDeath")
     * @JMSSerializer\Type("DateTime<'Y-m-d'>")
     */
    protected $dateOfDeath;

    /**
     * @var string $city
     *
     * @ORM\Column(type="string", name="city")
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your city name must be at least {{ limit }} characters long",
     *      maxMessage = "Your city name cannot be longer than {{ limit }} characters"
     * )
     * @JMSSerializer\Expose
     * @JMSSerializer\SerializedName("city")
     */
    protected $city;

    /**
     * @var Book
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Book", mappedBy="author", cascade={"persist", "remove"})
     */
    protected $book;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->book = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Author
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Author
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birthday
     *
     * @param \Date $birthday
     *
     * @return Author
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \Date
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set dateOfDeath
     *
     * @param \Date $dateOfDeath
     *
     * @return Author
     */
    public function setDateOfDeath($dateOfDeath)
    {
        $this->dateOfDeath = $dateOfDeath;

        return $this;
    }

    /**
     * Get dateOfDeath
     *
     * @return \Date
     */
    public function getDateOfDeath()
    {
        return $this->dateOfDeath;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Author
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add book
     *
     * @param \AppBundle\Entity\Book $book
     *
     * @return Author
     */
    public function addBook(\AppBundle\Entity\Book $book)
    {
        $this->book[] = $book;

        return $this;
    }

    /**
     * Remove book
     *
     * @param \AppBundle\Entity\Book $book
     */
    public function removeBook(\AppBundle\Entity\Book $book)
    {
        $this->book->removeElement($book);
    }

    /**
     * Get book
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBook()
    {
        return $this->book;
    }


}
