<?php
/**
 * Created by PhpStorm.
 * User: ProgKP
 * Date: 25.12.16 13:44
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMSSerializer;

/**
 * Class Book
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="book")
 */
class Book
{

    /**
     * @var integer $id
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string $nameBook
     * @ORM\Column(type="string", name="nameBook")
     *
     *
     * @Assert\NotBlank()
     * @JMSSerializer\Expose
     * @JMSSerializer\SerializedName("nameBook")
     */
    protected $nameBook;

    /**
     * @var string $colorBook
     * @ORM\Column(type="string", name="colorBook")
     *
     * @Assert\NotBlank()
     * @JMSSerializer\Expose
     * @JMSSerializer\SerializedName("colorBook")
     */
    protected $colorBook;

    /**
     * @var Author
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Author", inversedBy="book")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=false)
     * @JMSSerializer\Expose
     */
    protected $author;

    /**
     * @var date $datePublish
     * @ORM\Column(type="date", name="datePublish")
     *
     * @Assert\NotBlank()
     * @Assert\Expression(
     *     "value > this.getAuthor().getBirthday()",
     *     message = "Date of birth Author must be less than the date of Publish"
     * )
     * @JMSSerializer\Expose
     * @JMSSerializer\SerializedName("datePublish")
     * @JMSSerializer\Type("DateTime<'Y-m-d'>")
     */
    protected $datePublish;

    /**
     * @var string $cityPublish
     * @ORM\Column(type="string", name="cityPublish")
     *
     * @Assert\NotBlank()
     * @Assert\Expression(
     *     "value == this.getAuthor().getCity()",
     *     message="City of the author must be equal to the city books"
     * )
     * @JMSSerializer\Expose
     * @JMSSerializer\SerializedName("cityPublish")
     */
    protected $cityPublish;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameBook
     *
     * @param string $nameBook
     *
     * @return Book
     */
    public function setNameBook($nameBook)
    {
        $this->nameBook = $nameBook;

        return $this;
    }

    /**
     * Get nameBook
     *
     * @return string
     */
    public function getNameBook()
    {
        return $this->nameBook;
    }

    /**
     * Set colorBook
     *
     * @param string $colorBook
     *
     * @return Book
     */
    public function setColorBook($colorBook)
    {
        $this->colorBook = $colorBook;

        return $this;
    }

    /**
     * Get colorBook
     *
     * @return string
     */
    public function getColorBook()
    {
        return $this->colorBook;
    }

    /**
     * Set datePublish
     *
     * @param \DateTime $datePublish
     *
     * @return Book
     */
    public function setDatePublish($datePublish)
    {
        $this->datePublish = $datePublish;

        return $this;
    }

    /**
     * Get datePublish
     *
     * @return \Date
     */
    public function getDatePublish()
    {
        return $this->datePublish;
    }

    /**
     * Set cityPublish
     *
     * @param string $cityPublish
     *
     * @return Book
     */
    public function setCityPublish($cityPublish)
    {
        $this->cityPublish = $cityPublish;

        return $this;
    }

    /**
     * Get cityPublish
     *
     * @return string
     */
    public function getCityPublish()
    {
        return $this->cityPublish;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\Author $author
     *
     * @return Book
     */
    public function setAuthor(\AppBundle\Entity\Author $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\Author
     */
    public function getAuthor()
    {
        return $this->author;
    }


    /**
     * @return bool
     *
     * @Assert\IsTrue(
     *     message = "It is impossible to show, delete, or modify the book, whose authors are dead and with the death of 10 years had passed for now"
     * )
     */
    public function isDateRange10()
    {
        return (date_diff(new \DateTime('now'), $this->getAuthor()->getDateOfDeath())->y > 10);
    }
}
